import React, { Component } from 'react';
import './App.css';
import Dishes from './components/dishes.js';
import SignUp from './components/signup.js';
import Cart from './components/cart.js';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: undefined,
      formValues: {email: "", password: ""},
      section: "",
      loginResponse: "",
      signUp: "",
      selectedDishes: [],
      cart: []
    };
  }

  handleChange(event) {
    event.preventDefault();
    let formValues = this.state.formValues;
    let name = event.target.name;
    let value = event.target.value;

    formValues[name] = value;

    this.setState({formValues})
  }

handleLoginClick(){
      var self = this;
      axios.post('http://api-vanhack-event-sp.azurewebsites.net/api/v1/Customer/auth?email='+this.state.formValues.email+'&password='+this.state.formValues.password,
      {headers: {
        'Access-Control-Allow-Origin': '*',
      }})
      .then(function (response) {
        self.setState({token: response.data, section: ""});
      }).catch(function (error) {
        if (error.response.data.error !== undefined){
          self.setState({loginResponse: error.response.data.error});
        }
      });
  }

  handleAddToCart(){
    let selected = this.state.selectedDishes.slice(0);
    this.setState({
      cart: selected,
      selectedDishes: [],
      section: "cart"
    });
  }

  handleCartRemove(obj){
    let cart = this.state.cart.slice(0);
    let position = cart.indexOf(obj);
    if(position !== -1){
      console.log(cart);
      cart.splice(position, 1);
      console.log(cart);
      this.setState({cart: cart});
    }
  }

  handleDishSelected(obj){
    let selected = this.state.selectedDishes.slice(0);
    let position = selected.indexOf(obj);
    if(position === -1){
      selected[selected.length] = obj;
    } else {
      selected.splice(position, 1);
    }
    this.setState({selectedDishes: selected});
  }

  handleSignUpClick(formValues){
    let userData = JSON.stringify({
      "email": formValues.email,
      "name": formValues.name,
      "address": formValues.address,
      "password": formValues.password
    });

    var self = this;

    axios.post('http://api-vanhack-event-sp.azurewebsites.net/api/v1/Customer', userData,
    {
      headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json-patch+json'
      }
    })
    .then(function (response) {
      self.setState({signUpResponse: "Success!"});
      self.setState({token: response.data});
    }).catch(function (error) {
      if (error.response.data.error !== undefined){
        self.setState({signUpResponse: error.response.data.error});
      }
    });

  }

  handlePlaceOrder(){

  }

  render() {
    return (
      <div className="App">
        <h1>Skip the Dishes</h1>
          <div>
            <div>
              <div className="menu">
                <center>
                {this.state.token === undefined ? (
                  <div>
                    <button onClick={(evt) => {evt.stopPropagation(); this.setState({section: "signUp"});}}>Sign Up</button>
                  </div>
                ) : (
                  <div>
                    <button onClick={(evt) => {evt.stopPropagation(); this.setState({section: "dishes"});}}>Dishes</button>
                    <button onClick={(evt) => {evt.stopPropagation(); this.setState({section: "cart"});}}>Cart</button>
                  </div>

                )}
                </center>
              </div>
              <div style={{clear:'both'}}></div>
            </div>
            <br/>
            {this.state.token === undefined && (
            <div>
              <input type="text" placeholder="email" name="email" value={this.state.formValues.email} onChange={this.handleChange.bind(this)}></input>
              <input type="password" placeholder="password" name="password" value={this.state.formValues.password} onChange={this.handleChange.bind(this)}></input>
              <button onClick={() => {this.handleLoginClick()}}>Login</button><br/>
              {this.state.loginResponse}
            </div>
            )}
            <br/>
            {this.state.section === 'signUp' && <SignUp onSignUpClick={this.handleSignUpClick.bind(this)} response={this.state.signUpResponse}></SignUp>}
            {this.state.section === 'dishes' && <Dishes
              onCartSubmit={() => this.handleAddToCart.bind(this)}
              onSelectDish={(id) => this.handleDishSelected(id)}></Dishes>}
            {this.state.section === 'cart' && <Cart
              onClickRemove={(obj) => this.handleCartRemove(obj)}
              onSubmitOrder={() => this.handlePlaceOrder()}
              cart={this.state.cart}></Cart>}
          </div>
      </div>
    );
  }
}

export default App;
