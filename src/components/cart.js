import React, { Component } from 'react';
import axios from 'axios';

class Cart extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  handleChange(event) {
    event.preventDefault();
    let formValues = this.state.formValues;
    let name = event.target.name;
    let value = event.target.value;

    formValues[name] = value;

    this.setState({formValues})
  }

  render(){
    return (
            <div>
              <h2>Cart</h2>
              <div>
                <ul>
                {
                  this.props.cart.map((obj, i) =>
                    <li><button key={i} onClick={() => this.props.onClickRemove(obj)}>Remove</button> {obj.name}</li>
                  )
                }
                </ul>
                <button onClick={() => this.props.onSubmitOrder()}>Submit Order</button>
              </div>
            </div>
          )
  }
}

export default Cart;
