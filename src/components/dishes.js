import React, { Component } from 'react';
import axios from 'axios';

class Dishes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formValues: {},
      dishes: []
    };
  }

  render(){
    if(this.state.dishes.length === 0){
      var self = this;
      axios.get('http://api-vanhack-event-sp.azurewebsites.net/api/v1/Product',
      {
        headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json-patch+json'
        }
      })
      .then(function (response) {
        self.setState({dishes: response.data});
      }).catch(function (error) {
      });
    }

    return (
      <div>
        <h2>Our dishes</h2>
        <ul>
          {
            this.state.dishes.map((obj, i) =>
              <li><input type="checkbox" key={obj.id} value={obj.id} onChange={() => this.props.onSelectDish(obj)}/> {obj.name} (${obj.price})</li>
            )
          }
        </ul>
        <button onClick={this.props.onCartSubmit(this.state.selectedDishes)}>Add to cart</button>
      </div>
    )
  }
}

export default Dishes;
