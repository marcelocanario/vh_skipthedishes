import React, { Component } from 'react';
import axios from 'axios';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formValues: {},
      response: "",
      token: ""
    };
  }

  handleChange(event) {
    event.preventDefault();
    let formValues = this.state.formValues;
    let name = event.target.name;
    let value = event.target.value;

    formValues[name] = value;

    this.setState({formValues})
  }

  render(){
    return (<div>
              <input type="text" placeholder="Name" name="name" value={this.state.fieldName} onChange={this.handleChange.bind(this)}></input><br/>
              <input type="text" placeholder="E-mail" name="email" value={this.state.fieldEmail} onChange={this.handleChange.bind(this)}></input><br/>
              <input type="password" placeholder="Password" name="password" value={this.state.fieldPassword} onChange={this.handleChange.bind(this)}></input><br/>
              <input type="text" placeholder="Address" name="address" value={this.state.fieldAddress} onChange={this.handleChange.bind(this)}></input><br/>
              <button onClick={() => {this.props.onSignUpClick(this.state.formValues)}}>Sign Up</button><br/><br/>
              {this.props.response}
            </div>)
  }
}

export default SignUp;
